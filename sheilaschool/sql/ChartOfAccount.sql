INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (10000, 10000, 'Saldo', '    ', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (11000, 11000, 'Rekening Bank', '    ', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (11100, 11100, 'Rekening Bank - Khusus', 'Saldo yang tersimpan di rekening bank, untuk kebutuhan/pembiayaan khusus, seperti rekening untuk zakat fithrah, hanya untuk fakir miskin saja.', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (11200, 11200, 'Rekening Bank - Umum', 'Saldo yang tersimpan di rekening bank, dan memang jelas bebas digunakan untuk institusi sesuai aturan yang berlaku,', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (12000, 12000, 'Kas', '    ', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (12100, 12100, 'Kas - Umum', 'Saldo yang disimpan pengurus, untuk kebutuhan umum seperti saldo kotak amal Jumat', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (12200, 12200, 'Kas - Khusus', 'Saldo yang disimpan pengurus, untuk kebutuhan khusus seperti saldo kas untuk operasional dan konsumsi petugas', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (19000, 19000, 'Saldo Lainnya', 'Saldo lainnya dalam bentuk non cash yang dihargai dalam rupiah.', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (40000, 40000, 'Penerimaan', '    ', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (41000, 41000, 'Donasi Institusi', '    ', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (41010, 41010, 'Donasi Pemerintah', 'Donasi dari pemerintah misalnya seperti BLT', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (41020, 41020, 'Donasi Swasta (CSR)', 'Donasi dari swasta seperti dari dana CSR atau sumbangan karyawan.', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (42000, 42000, 'Donasi Individu', '    ', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (42010, 42010, 'Donasi Individu Tercatat', 'Donasi dari individu yang tercatat sumber dan identitas pengirim', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (42020, 42020, 'Donasi Anonymous', 'Donasi dari individu yang tidak diketahui sumber nya atau dinyatakan anonymous.', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (43000, 43000, 'Penerimaan Hasil Usaha', 'Rekapitulasi segala Penerimaan institusi dari hasil usaha baik usaha internal maupun kerjasama', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (43010, 43010, 'Hasil penjualan', 'Penerimaan institusi dari kegiatan penjualan barang', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (43020, 43020, 'Hasil upah jasa', 'Penerimaan institusi dari kegiatan upah atau jasa', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (43099, 43099, 'Bonus/ Hasil usaha lain-lain', 'Penerimaan institusi dari usaha lain-lain atau bonus/komisi', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (44000, 44000, 'Iuran', 'Penerimaan dalam bentuk iuran rutin, seperti iuran kebersihan.', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (49000, 49000, 'Penerimaan Lainnya', 'Penerimaan yang belum bisa dikategorikan.', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (60000, 60000, 'Pengeluaran', '    ', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (61000, 61000, 'Penyaluran donasi', 'Pembiayaan dalam bentuk menyalurkan kembali donasi yang diterima kepada pihak lain, seperti penyaluran zakat maal kepada para mustahiq.', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (62000, 62000, 'Gaji dan Honor', 'Pembiayaan untuk gaji dan honor, seperti gaji marbot, gaji keamanan, honor pembicaran, honor ustadz, moderator', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (63000, 63000, 'Biaya Umum', 'Biaya untuk kebutuhan umum seperti administrasi, iuran kebersihan, dan keamanan', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (63400, 63400, 'Biaya Administrasi', 'Pembiayaan administrasi seperti biaya pengurusan akta notaris, biaya transfer dll', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (64000, 64000, 'Biaya Sewa dan Jasa', 'Pembiayaan sewa ruangan, alat, sewa jasa, termasuk jasa biaya tranportasi, kurir, akomodasi', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (65000, 65000, 'Pengeluaran Kegiatan/Program', 'Pengeluaran yang terkait dengan kegiatan atau program institusi', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (65100, 65100, 'Biaya Publikasi', 'Pembiayaan untuk publikasi program, kegiatan', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (65200, 65200, 'Biaya Konsumsi', 'Pembiayaan untuk konsumsi kegiatan/program', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (65400, 65400, 'Biaya Peralatan', 'Pembiayaan pembeliaan peralatan untuk kegiatan atau untuk kepentingan ins', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (66000, 66000, 'Pengeluaran Umum/Rutin', 'Segala pembiayaan/ pengeluaran yang rutin terjadi atau tidak terikat program/kegiatan khusus', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (66100, 66100, 'Biaya Bahan Habis Pakai', 'Pembiayaan bahan habis pakai yang rutin digunakan, seperti printer, sabun, cairan pembersih.', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (66200, 66200, 'Biaya Operasional', 'Pembiayaan rutin operasional, seperti listrik, air, internet', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (66300, 66300, 'Biaya Perawatan', 'Pembiayaan terkait perawatan aset institusi, termasuk cat tembok rutin.', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (67000, 67000, 'Pengembangan SDM/ Pelatihan', 'Pembiayaan Pengembangan Sumber daya Insitutusi, seperti pelatihan, beasiswa, buku.', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (68000, 68000, 'Biaya Renovasi/ Reparasi', 'Pembiayaan renovasi, perbaikan kerusakan bangunan, alat, kendaraan', '    ');
INSERT INTO public.chartofaccount (id, code, name, description, reference) VALUES (69000, 69000, 'Biaya-biaya Lainnya', 'Pembiayaan lain yang belum terkategorikan atau belum jelas kategorinya', '    ');