package aisco.product.sheilaschool.model;

import java.util.HashMap;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="automaticreport_periodic")
public class AutomaticReportPeriodic {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="seq")
    @GenericGenerator(name = "seq", strategy="increment")
    private int id;

    private String name;
    private Boolean isActive;

    public AutomaticReportPeriodic(String name, Boolean isActive) {
        this.name = name;
        this.isActive = isActive;
    }

    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> automaticReportPeriodicMap = new HashMap<String,Object>();
        automaticReportPeriodicMap.put("id", getId());
        automaticReportPeriodicMap.put("name", getName());
        automaticReportPeriodicMap.put("isActive", getIsActive());
        return automaticReportPeriodicMap;
    }
}
