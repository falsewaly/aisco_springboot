package aisco.product.sheilaschool.resource;

import aisco.product.sheilaschool.model.FinancialReportExpense;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import aisco.product.sheilaschool.model.ChartOfAccount;
import aisco.product.sheilaschool.model.FinancialReportIncome;
import aisco.product.sheilaschool.model.Program;
import aisco.product.sheilaschool.repository.ChartOfAccountRepository;
import aisco.product.sheilaschool.repository.FinancialReportIncomeRepository;
import aisco.product.sheilaschool.repository.ProgramRepository;

@RestController
public class FinancialReportIncomeResource {

	@Autowired
    private FinancialReportIncomeRepository repository;
    @Autowired
    private ProgramRepository programRepository;
    @Autowired
    private ChartOfAccountRepository coaRepository;

    @PostMapping("call/income/save")
    public List<HashMap<String, Object>> saveFinancialReportIncome(@RequestBody HashMap<String, String>
    financialReportData) {
        String datestamp = financialReportData.get("datestamp");
        long amount = Long.parseLong(financialReportData.get("amount"));
        String description = financialReportData.get("description");
        Program program = programRepository.findById(Integer.parseInt(financialReportData
            .get("idProgram"))).orElse(null);
        ChartOfAccount coa = coaRepository.findById(Integer.parseInt(financialReportData
            .get("idCoa"))).orElse(null);
        String paymentMethod = financialReportData.get("paymentMethod");
        FinancialReportIncome financialReport = new FinancialReportIncome(
            datestamp, amount, description, program, coa, paymentMethod
        );

        repository.save(financialReport);
        return getAllFinancialReportIncome();
    }

    @PutMapping("call/income/update")
    public HashMap<String, Object> updateFinancialReportIncome(@RequestBody HashMap<String, String>
                                                                financialReport) {
    	FinancialReportIncome existingFinancialReport =
                repository.findById(Integer.parseInt(financialReport.get("id")))
                    .orElse(null);
        existingFinancialReport.setDatestamp(financialReport.get("datestamp"));
        existingFinancialReport.setAmount(Long.parseLong(financialReport.get("amount")));
        existingFinancialReport.setDescription(financialReport.get("description"));
        Program program = programRepository.findById(Integer.parseInt(financialReport
            .get("idProgram"))).orElse(null);
        ChartOfAccount coa = coaRepository.findById(Integer.parseInt(financialReport
            .get("idCoa"))).orElse(null);

        existingFinancialReport.setProgram(program);
        existingFinancialReport.setCoa(coa);
        existingFinancialReport.setPaymentMethod(financialReport.get("paymentMethod"));
        repository.save(existingFinancialReport);
        return existingFinancialReport.toHashMap();
    }

    @GetMapping("call/income/detail")
    public HashMap<String, Object> getFinancialReportIncome(@RequestParam int id) {
        FinancialReportIncome income = repository.findById(id).orElse(null);
        return income.toHashMap();
    }

    @GetMapping("call/income/list")
    public List<HashMap<String, Object>> getAllFinancialReportIncome() {
        List<FinancialReportIncome> incomes = repository.findAll();
        List<HashMap<String, Object>> incomeResponse = new ArrayList<HashMap<String, Object>>();
        for (FinancialReportIncome expense : incomes) {
            incomeResponse.add(expense.toHashMap());
        }
        return incomeResponse;
    }

    @DeleteMapping("call/income/delete")
    public List<HashMap<String, Object>> deleteFinancialReportIncome(@RequestBody HashMap<String,
        String> idMap) {
        int id = Integer.parseInt(idMap.get("id"));
        repository.deleteById(id);
        return getAllFinancialReportIncome();
    }

}
