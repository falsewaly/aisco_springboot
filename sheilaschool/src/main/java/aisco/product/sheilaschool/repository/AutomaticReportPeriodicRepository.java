package aisco.product.sheilaschool.repository;

import aisco.product.sheilaschool.model.AutomaticReportPeriodic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AutomaticReportPeriodicRepository extends JpaRepository<AutomaticReportPeriodic, Integer> {
}
