package aisco.product.sheilaschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import aisco.product.sheilaschool.model.FinancialReportExpense;

public interface FinancialReportExpenseRepository extends JpaRepository<FinancialReportExpense, Integer>{

}
