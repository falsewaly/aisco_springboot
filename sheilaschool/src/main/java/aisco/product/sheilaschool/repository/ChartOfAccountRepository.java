package aisco.product.sheilaschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import aisco.product.sheilaschool.model.ChartOfAccount;

public interface ChartOfAccountRepository extends JpaRepository<ChartOfAccount, Integer>{

}
