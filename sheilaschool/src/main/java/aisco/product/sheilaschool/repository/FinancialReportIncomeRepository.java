package aisco.product.sheilaschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import aisco.product.sheilaschool.model.FinancialReportIncome;

public interface FinancialReportIncomeRepository extends JpaRepository<FinancialReportIncome, Integer>{

}
