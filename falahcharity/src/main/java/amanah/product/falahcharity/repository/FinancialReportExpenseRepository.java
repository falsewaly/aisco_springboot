package amanah.product.falahcharity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import amanah.product.falahcharity.model.FinancialReportExpense;

public interface FinancialReportExpenseRepository extends JpaRepository<FinancialReportExpense, Integer>{

}
