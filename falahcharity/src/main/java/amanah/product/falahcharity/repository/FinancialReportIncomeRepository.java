package amanah.product.falahcharity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import amanah.product.falahcharity.model.FinancialReportIncome;

public interface FinancialReportIncomeRepository extends JpaRepository<FinancialReportIncome, Integer>{

}
