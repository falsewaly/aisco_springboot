package amanah.product.falahcharity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import amanah.product.falahcharity.model.FinancialReportIncomeFrequency;

public interface FinancialReportIncomeFrequencyRepository extends JpaRepository<FinancialReportIncomeFrequency, Integer>{

}
