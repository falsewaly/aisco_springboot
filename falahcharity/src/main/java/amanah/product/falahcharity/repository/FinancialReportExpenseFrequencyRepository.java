package amanah.product.falahcharity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import amanah.product.falahcharity.model.FinancialReportExpenseFrequency;

public interface FinancialReportExpenseFrequencyRepository extends JpaRepository<FinancialReportExpenseFrequency, Integer>{

}
