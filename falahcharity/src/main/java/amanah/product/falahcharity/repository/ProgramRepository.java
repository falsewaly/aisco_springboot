package amanah.product.falahcharity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import amanah.product.falahcharity.model.Program;

public interface ProgramRepository extends JpaRepository<Program, Integer>{

}
