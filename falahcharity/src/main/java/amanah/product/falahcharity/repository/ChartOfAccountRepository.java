package amanah.product.falahcharity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import amanah.product.falahcharity.model.ChartOfAccount;

public interface ChartOfAccountRepository extends JpaRepository<ChartOfAccount, Integer>{

}
