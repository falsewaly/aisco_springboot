package amanah.product.falahcharity.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import amanah.product.falahcharity.model.ChartOfAccount;
import amanah.product.falahcharity.model.FinancialReportIncome;
import amanah.product.falahcharity.model.FinancialReportIncomeFrequency;
import amanah.product.falahcharity.model.Program;
import amanah.product.falahcharity.repository.ChartOfAccountRepository;
import amanah.product.falahcharity.repository.FinancialReportIncomeFrequencyRepository;
import amanah.product.falahcharity.repository.FinancialReportIncomeRepository;
import amanah.product.falahcharity.repository.ProgramRepository;

@RestController
public class FinancialReportIncomeFrequencyResource {

	@Autowired
    private FinancialReportIncomeFrequencyRepository repository;
    @Autowired
    private ProgramRepository programRepository;
    @Autowired
    private ChartOfAccountRepository coaRepository;

    @PostMapping("call/income/save")
    public List<HashMap<String, Object>> saveFinancialReportIncome(@RequestBody HashMap<String, String>
    financialReportData) {
        String datestamp = financialReportData.get("datestamp");
        long amount = Long.parseLong(financialReportData.get("amount"));
        String description = financialReportData.get("description");
        Program program = programRepository.findById(Integer.parseInt(financialReportData
            .get("idProgram"))).orElse(null);
        ChartOfAccount coa = coaRepository.findById(Integer.parseInt(financialReportData
            .get("idCoa"))).orElse(null);
        String paymentMethod = financialReportData.get("paymentMethod");
        String frequency= financialReportData.get("frequency");
        FinancialReportIncomeFrequency financialReport = new FinancialReportIncomeFrequency(
            datestamp, amount, description, program, coa, paymentMethod, frequency
        );

        repository.save(financialReport);
        return getAllFinancialReportIncome();
    }

    @PutMapping("call/income/update")
    public HashMap<String, Object> updateFinancialReportIncome(@RequestBody HashMap<String, String>
                                                                financialReport) {
    	FinancialReportIncomeFrequency existingFinancialReport =
                repository.findById(Integer.parseInt(financialReport.get("id")))
                    .orElse(null);
        existingFinancialReport.setDatestamp(financialReport.get("datestamp"));
        existingFinancialReport.setAmount(Long.parseLong(financialReport.get("amount")));
        existingFinancialReport.setDescription(financialReport.get("description"));
        Program program = programRepository.findById(Integer.parseInt(financialReport
            .get("idProgram"))).orElse(null);
        ChartOfAccount coa = coaRepository.findById(Integer.parseInt(financialReport
            .get("idCoa"))).orElse(null);

        existingFinancialReport.setProgram(program);
        existingFinancialReport.setCoa(coa);
        existingFinancialReport.setPaymentMethod(financialReport.get("paymentMethod"));
        existingFinancialReport.setFrequency(financialReport.get("frequency"));
        repository.save(existingFinancialReport);
        return existingFinancialReport.toHashMap();
    }

    @GetMapping("call/income/detail")
    public HashMap<String, Object> getFinancialReportIncome(@RequestParam int id) {
    	FinancialReportIncomeFrequency incomeFrequency = repository.findById(id).orElse(null);
        return incomeFrequency.toHashMap();
    }

    @GetMapping("call/income/list")
    public List<HashMap<String, Object>> getAllFinancialReportIncome() {
        List<FinancialReportIncomeFrequency> incomeFrequencies = repository.findAll();
        List<HashMap<String, Object>> incomeResponse = new ArrayList<HashMap<String, Object>>();
        for (FinancialReportIncome expense : incomeFrequencies) {
            incomeResponse.add(expense.toHashMap());
        }
        return incomeResponse;
    }

    @DeleteMapping("call/income/delete")
    public List<HashMap<String, Object>> deleteFinancialReportIncome(@RequestBody HashMap<String,
        String> idMap) {
        int id = Integer.parseInt(idMap.get("id"));
        repository.deleteById(id);
        return getAllFinancialReportIncome();
    }

}
