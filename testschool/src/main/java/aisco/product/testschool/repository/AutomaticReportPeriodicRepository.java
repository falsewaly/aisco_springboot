package aisco.product.testschool.repository;

import aisco.product.testschool.model.AutomaticReportPeriodic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AutomaticReportPeriodicRepository extends JpaRepository<AutomaticReportPeriodic, Integer> {
}
