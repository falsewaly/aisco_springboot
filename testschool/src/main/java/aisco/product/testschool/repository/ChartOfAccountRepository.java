package aisco.product.testschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import aisco.product.testschool.model.ChartOfAccount;

public interface ChartOfAccountRepository extends JpaRepository<ChartOfAccount, Integer>{

}
