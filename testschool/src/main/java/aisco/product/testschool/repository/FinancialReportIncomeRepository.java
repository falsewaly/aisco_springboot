package aisco.product.testschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import aisco.product.testschool.model.FinancialReportIncome;

public interface FinancialReportIncomeRepository extends JpaRepository<FinancialReportIncome, Integer>{

}
