package aisco.product.testschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import aisco.product.testschool.model.Program;

public interface ProgramRepository extends JpaRepository<Program, Integer>{

}
