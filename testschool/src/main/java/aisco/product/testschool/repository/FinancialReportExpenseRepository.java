package aisco.product.testschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import aisco.product.testschool.model.FinancialReportExpense;

public interface FinancialReportExpenseRepository extends JpaRepository<FinancialReportExpense, Integer>{

}
