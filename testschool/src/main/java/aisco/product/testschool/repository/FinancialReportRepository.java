package aisco.product.testschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import aisco.product.testschool.model.FinancialReport;

public interface FinancialReportRepository extends JpaRepository<FinancialReport, Integer> {
}
