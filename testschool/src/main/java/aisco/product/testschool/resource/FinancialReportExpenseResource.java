package aisco.product.testschool.resource;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import aisco.product.testschool.model.ChartOfAccount;
import aisco.product.testschool.model.FinancialReportExpense;
import aisco.product.testschool.model.Program;
import aisco.product.testschool.repository.ChartOfAccountRepository;
import aisco.product.testschool.repository.FinancialReportExpenseRepository;
import aisco.product.testschool.repository.ProgramRepository;

@RestController
public class FinancialReportExpenseResource {

	@Autowired
    private FinancialReportExpenseRepository repository;
    @Autowired
    private ProgramRepository programRepository;
    @Autowired
    private ChartOfAccountRepository coaRepository;

    @PostMapping("call/expense/save")
    public List<FinancialReportExpense> saveFinancialReportExpense(@RequestBody HashMap<String, String>
    financialReportData) {
    	 String datestamp = financialReportData.get("datestamp");
         long amount = Long.parseLong(financialReportData.get("amount"));
         String description = financialReportData.get("description");
         Program program = programRepository.findById(Integer.parseInt(financialReportData
             .get("idProgram"))).orElse(null);
         ChartOfAccount coa = coaRepository.findById(Integer.parseInt(financialReportData
             .get("idCoa"))).orElse(null);
         FinancialReportExpense financialReport = new FinancialReportExpense(
             datestamp, amount, description, program, coa
         );
        repository.save(financialReport);
        return getAllFinancialReportExpense();
    }

    @PutMapping("call/expense/update")
    public FinancialReportExpense updateFinancialReportExpense(@RequestBody HashMap<String, String>
                                                                financialReport) {
    	FinancialReportExpense existingFinancialReport =
                repository.findById(Integer.parseInt(financialReport.get("id")))
                    .orElse(null);
        existingFinancialReport.setDatestamp(financialReport.get("datestamp"));
        existingFinancialReport.setAmount(Long.parseLong(financialReport.get("amount")));
        existingFinancialReport.setDescription(financialReport.get("description"));
        Program program = programRepository.findById(Integer.parseInt(financialReport
            .get("idProgram"))).orElse(null);
        ChartOfAccount coa = coaRepository.findById(Integer.parseInt(financialReport
            .get("idCoa"))).orElse(null);

        existingFinancialReport.setProgram(program);
		existingFinancialReport.setCoa(coa);
		return repository.save(existingFinancialReport);
    }

    @GetMapping("call/expense/detail")
    public FinancialReportExpense getFinancialReportExpense(@RequestParam int id) {
    	return repository.findById(id).orElse(null);
    }

    @GetMapping("call/expense/list")
    public List<FinancialReportExpense> getAllFinancialReportExpense() {
    	return repository.findAll();
    }

    @DeleteMapping("call/expense/delete")
    public List<FinancialReportExpense> deleteFinancialReportExpense(@RequestBody HashMap<String,
        String> idMap) {
    	int id = Integer.parseInt(idMap.get("id"));
        repository.deleteById(id);
        return getAllFinancialReportExpense();
    }

}
