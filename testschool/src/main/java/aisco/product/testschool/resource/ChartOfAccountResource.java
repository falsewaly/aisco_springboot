package aisco.product.testschool.resource;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import aisco.product.testschool.model.ChartOfAccount;
import aisco.product.testschool.repository.ChartOfAccountRepository;

@RestController
public class ChartOfAccountResource {

	@Autowired
	private ChartOfAccountRepository repository;
	
	@PostMapping("call/chart-of-account/save")
    public List<ChartOfAccount> saveChartOfAccount(@RequestBody ChartOfAccount chartOfAccount) {
		repository.save(chartOfAccount);
        return getAllChartOfAccount();
    }

	@PutMapping("call/chart-of-account/update")
    public ChartOfAccount updateChartOfAccount(@RequestBody ChartOfAccount chartOfAccount) {
		ChartOfAccount existingChartOfAccount = repository.findById(chartOfAccount.getId()).orElse(null);
		existingChartOfAccount.setName(chartOfAccount.getName());
		existingChartOfAccount.setCode(chartOfAccount.getCode());
		existingChartOfAccount.setDescription(chartOfAccount.getDescription());
		existingChartOfAccount.setReference(chartOfAccount.getReference());
		return repository.save(existingChartOfAccount);
    }

    @GetMapping("call/chart-of-account/detail")
    public ChartOfAccount getChartOfAccount(@RequestParam int id) {
    	return repository.findById(id).orElse(null);
    }

    @GetMapping("call/chart-of-account/list")
    public List<ChartOfAccount> getAllChartOfAccount() {
    	return repository.findAll();
    }

    @DeleteMapping("call/chart-of-account/delete")
    public List<ChartOfAccount> deleteChartOfAccount(@RequestBody HashMap<String,
            String> idMap) {
        int id = Integer.parseInt(idMap.get("id"));
    	repository.deleteById(id);
        return getAllChartOfAccount();
    }
	
}
