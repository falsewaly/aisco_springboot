package aisco.product.testschool.resource;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import aisco.product.testschool.model.FinancialReportExpense;
import aisco.product.testschool.model.FinancialReportIncome;
import aisco.product.testschool.repository.FinancialReportExpenseRepository;
import aisco.product.testschool.repository.FinancialReportIncomeRepository;

@RestController
public class SummaryResource {
	@Autowired
	private FinancialReportIncomeRepository incomeRepository;
	@Autowired
	private FinancialReportExpenseRepository expenseRepository;

	@GetMapping("call/summary/list")
	public List<HashMap<String, Object>> list() {
		List<FinancialReportIncome> allIncome = incomeRepository.findAll();
		List<FinancialReportExpense> allExpense = expenseRepository.findAll();
		List<HashMap<String, Object>> summariesWithSaldo = new ArrayList<HashMap<String, Object>>();

		long saldo = 0;

		for (int i = 0; i < allIncome.size(); i++) {
			FinancialReportIncome frIncome = allIncome.get(i);
			HashMap<String, Object> summaryMap = new HashMap<String, Object>();
			String datestamp = frIncome.getDatestamp();
			summaryMap.put("datestamp", datestamp);
			String description = frIncome.getDescription();
			summaryMap.put("description", description);
			long income = frIncome.getAmount();
			summaryMap.put("income", income);
			long expense = 0;
			summaryMap.put("expense", expense);
			String programName = frIncome.getProgram().getName();
			summaryMap.put("programName", programName);
			saldo = saldo + income;
			summaryMap.put("saldo", saldo);
			summariesWithSaldo.add(summaryMap);
		}
		
		for (int i = 0; i < allExpense.size(); i++) {
			FinancialReportExpense frExpense = allExpense.get(i);
			HashMap<String, Object> summaryMap = new HashMap<String, Object>();
			String datestamp = frExpense.getDatestamp();
			summaryMap.put("datestamp", datestamp);
			String description = frExpense.getDescription();
			summaryMap.put("description", description);
			long income = 0;
			summaryMap.put("income", income);
			long expense = frExpense.getAmount();
			summaryMap.put("expense", expense);
			String programName = frExpense.getProgram().getName();
			summaryMap.put("programName", programName);
			saldo = saldo + income;
			summaryMap.put("saldo", saldo);
			summariesWithSaldo.add(summaryMap);
		}

		// sort berdasarkan tanggal
		Collections.sort(summariesWithSaldo, new Comparator<HashMap<String, Object>>() {
            @Override
            public int compare(final HashMap<String, Object> o1, final HashMap<String, Object> o2) {
                String sDate1 = o1.get("datestamp").toString();
                String sDate2 = o2.get("datestamp").toString();
                Date date1 = null;
                Date date2 = null;
                try {
                    date1 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
                    date2 = new SimpleDateFormat("yyyy-MM-dd").parse(sDate2);
                } catch (ParseException pe) {
                    System.out.println(pe.getMessage());
                }
                // sort descending
                return date2.compareTo(date1);
            }
        });

        return summariesWithSaldo;
    }

}
