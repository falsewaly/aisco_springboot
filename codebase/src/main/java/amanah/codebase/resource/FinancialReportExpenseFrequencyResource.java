package amanah.codebase.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import amanah.codebase.model.ChartOfAccount;
import amanah.codebase.model.FinancialReportExpense;
import amanah.codebase.model.FinancialReportExpenseFrequency;
import amanah.codebase.model.Program;
import amanah.codebase.repository.ChartOfAccountRepository;
import amanah.codebase.repository.FinancialReportExpenseFrequencyRepository;
import amanah.codebase.repository.FinancialReportExpenseRepository;
import amanah.codebase.repository.ProgramRepository;

@RestController
public class FinancialReportExpenseFrequencyResource {

	@Autowired
    private FinancialReportExpenseFrequencyRepository repository;
    @Autowired
    private ProgramRepository programRepository;
    @Autowired
    private ChartOfAccountRepository coaRepository;

    @PostMapping("call/income/save")
    public List<HashMap<String, Object>> saveFinancialReportExpense(@RequestBody HashMap<String, String>
    financialReportData) {
        String datestamp = financialReportData.get("datestamp");
        long amount = Long.parseLong(financialReportData.get("amount"));
        String description = financialReportData.get("description");
        Program program = programRepository.findById(Integer.parseInt(financialReportData
            .get("idProgram"))).orElse(null);
        ChartOfAccount coa = coaRepository.findById(Integer.parseInt(financialReportData
            .get("idCoa"))).orElse(null);
        String frequency= financialReportData.get("frequency");
        FinancialReportExpenseFrequency financialReport = new FinancialReportExpenseFrequency(
            datestamp, amount, description, program, coa, frequency
        );

        repository.save(financialReport);
        return getAllFinancialReportExpense();
    }

    @PutMapping("call/income/update")
    public HashMap<String, Object> updateFinancialReportExpense(@RequestBody HashMap<String, String>
                                                                financialReport) {
    	FinancialReportExpenseFrequency existingFinancialReport =
                repository.findById(Integer.parseInt(financialReport.get("id")))
                    .orElse(null);
        existingFinancialReport.setDatestamp(financialReport.get("datestamp"));
        existingFinancialReport.setAmount(Long.parseLong(financialReport.get("amount")));
        existingFinancialReport.setDescription(financialReport.get("description"));
        Program program = programRepository.findById(Integer.parseInt(financialReport
            .get("idProgram"))).orElse(null);
        ChartOfAccount coa = coaRepository.findById(Integer.parseInt(financialReport
            .get("idCoa"))).orElse(null);

        existingFinancialReport.setProgram(program);
        existingFinancialReport.setCoa(coa);
        existingFinancialReport.setFrequency(financialReport.get("frequency"));
        repository.save(existingFinancialReport);
        return existingFinancialReport.toHashMap();
    }

    @GetMapping("call/income/detail")
    public HashMap<String, Object> getFinancialReportExpense(@RequestParam int id) {
    	FinancialReportExpenseFrequency incomeFrequency = repository.findById(id).orElse(null);
        return incomeFrequency.toHashMap();
    }

    @GetMapping("call/income/list")
    public List<HashMap<String, Object>> getAllFinancialReportExpense() {
        List<FinancialReportExpenseFrequency> incomeFrequencies = repository.findAll();
        List<HashMap<String, Object>> incomeResponse = new ArrayList<HashMap<String, Object>>();
        for (FinancialReportExpense expense : incomeFrequencies) {
            incomeResponse.add(expense.toHashMap());
        }
        return incomeResponse;
    }

    @DeleteMapping("call/income/delete")
    public List<HashMap<String, Object>> deleteFinancialReportExpense(@RequestBody HashMap<String,
        String> idMap) {
        int id = Integer.parseInt(idMap.get("id"));
        repository.deleteById(id);
        return getAllFinancialReportExpense();
    }

}
