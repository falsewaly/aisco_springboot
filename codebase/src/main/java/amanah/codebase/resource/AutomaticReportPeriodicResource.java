package amanah.codebase.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import amanah.codebase.model.AutomaticReportPeriodic;
import amanah.codebase.model.ChartOfAccount;
import amanah.codebase.model.FinancialReportExpense;
import amanah.codebase.model.Program;
import amanah.codebase.repository.AutomaticReportPeriodicRepository;

@RestController
public class AutomaticReportPeriodicResource extends AbstractAutomaticReportResource {
    @Autowired
    private AutomaticReportPeriodicRepository automaticReportPeriodicRepository;

    @Override
    public List<HashMap<String, Object>> list() {
        return list(1);
    }

    @PostMapping("call/automatic-report-periodic-model/save")
    public List<AutomaticReportPeriodic> saveAutomaticReportPeriodic(@RequestBody HashMap<String, String>
                                                                       automaticReportPeriodicData) {
        String name = automaticReportPeriodicData.get("name");
        Boolean isActive = Boolean.parseBoolean(automaticReportPeriodicData.get("isActive"));
        AutomaticReportPeriodic automaticReportPeriodic =
            new AutomaticReportPeriodic(name, isActive);
        automaticReportPeriodicRepository.save(automaticReportPeriodic);
        return getAllAutomaticReportPeriodic();
    }

    @PutMapping("call/automatic-report-periodic-model/update")
    public AutomaticReportPeriodic updateAutomaticReportPeriodic(@RequestBody HashMap<String, String>
                                                                         automaticReportPeriodic) {
        AutomaticReportPeriodic existingAutomaticReportPeriodic =
            automaticReportPeriodicRepository
                .findById(Integer.parseInt(automaticReportPeriodic.get("id")))
                .orElse(null);
        existingAutomaticReportPeriodic.setName(automaticReportPeriodic.get("name"));
        existingAutomaticReportPeriodic.setIsActive(Boolean.parseBoolean(automaticReportPeriodic
            .get("isActive")));
        return automaticReportPeriodicRepository.save(existingAutomaticReportPeriodic);
    }

    @GetMapping("call/automatic-report-periodic-model/detail")
    public AutomaticReportPeriodic getAutomaticReportPeriodic(@RequestParam int id) {
        return automaticReportPeriodicRepository.findById(id).orElse(null);
    }

    @GetMapping("call/automatic-report-periodic-model/list")
    public List<AutomaticReportPeriodic> getAllAutomaticReportPeriodic() {
        return automaticReportPeriodicRepository.findAll();
    }

    @DeleteMapping("call/automatic-report-periodic-model/delete")
    public List<AutomaticReportPeriodic> deleteAutomaticReportPeriodic(@RequestBody HashMap<String,
        String> idMap) {
        int id = Integer.parseInt(idMap.get("id"));
        automaticReportPeriodicRepository.deleteById(id);
        return getAllAutomaticReportPeriodic();
    }

    @GetMapping("call/automatic-report-periodic/list")
    public List<HashMap<String, Object>> list(@RequestParam int id) {
        AutomaticReportPeriodic automaticReportPeriodic =
            automaticReportPeriodicRepository.findById(id).orElse(null);
        int year = Integer.parseInt(automaticReportPeriodic.getName()) - 1;

        List<HashMap<String, Object>> allIncomes =
            filterFinancialReportByPeriod(getIncomes(), year);
        List<HashMap<String, Object>> allExpenses =
            filterFinancialReportByPeriod(getExpenses(), year);

        List<HashMap<String,Object>> coaSheets = new ArrayList<HashMap<String, Object>>();
        List<HashMap<String,Object>> coaLevel1 = transFormToChartOfAccount(allIncomes, allExpenses, 1);
        List<HashMap<String,Object>> coaLevel2 = transFormToChartOfAccount(allIncomes, allExpenses, 2);

        try {
            coaSheets = getOperationalActivity(coaLevel1, coaLevel2);
            List<HashMap<String,Object>> investActivity = getInvestActivity();
            List<HashMap<String,Object>> fundingActivity = getFundingActivity();
            List<HashMap<String,Object>> cashFlowSummary = getCashFlowSummary(coaLevel1);
            HashMap<String, Object> emptyRow = this.createCoa("", 0);
            coaSheets = this.removeAmount(coaSheets, 1);

            coaSheets.addAll(investActivity);
            coaSheets.addAll(fundingActivity);
            coaSheets.add(emptyRow);
            coaSheets.addAll(cashFlowSummary);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return coaSheets;
    }

    private List<HashMap<String,Object>> filterFinancialReportByPeriod(
        List<HashMap<String, Object>> financialReports, int year) {
        List<HashMap<String, Object>> validFinancialReports = new ArrayList<HashMap<String, Object>>();
        for (HashMap<String, Object> financialReport: financialReports) {
            String datestamp = (String) financialReport.get("datestamp");
            int financialReportYear = Integer.parseInt(datestamp.split("-")[0]);
            if (year == financialReportYear) {
                validFinancialReports.add(financialReport);
            }
        }

        return validFinancialReports;
    }
}
