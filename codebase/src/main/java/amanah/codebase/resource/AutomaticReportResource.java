package amanah.codebase.resource;

import java.util.HashMap;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AutomaticReportResource extends AbstractAutomaticReportResource {

    @Override
    @GetMapping("call/automatic-report/list")
    public List<HashMap<String, Object>> list() {
        List<HashMap<String, Object>> allIncomes = this.getIncomes();
        List<HashMap<String, Object>> allExpenses = this.getExpenses();

        List<HashMap<String,Object>> coaLevel1 =
            transFormToChartOfAccount(allIncomes, allExpenses, 1);

        try {
            coaLevel1 = this.sort(coaLevel1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return coaLevel1;
    }
}
