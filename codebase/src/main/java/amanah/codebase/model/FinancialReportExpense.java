package amanah.codebase.model;

import java.util.HashMap;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@Entity
@Table(name="financialreport_expense")
public class FinancialReportExpense extends FinancialReport {

    public FinancialReportExpense(String datestamp, long amount,
                                 String description, Program program, ChartOfAccount coa) {
        this.datestamp = datestamp;
        this.amount = amount;
        this.description = description;
        this.program = program;
        this.coa = coa;
    }

    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> financialReportMap = new HashMap<String,Object>();
        financialReportMap.put("id", getId());
        financialReportMap.put("datestamp", getDatestamp());
        financialReportMap.put("amount", getAmount());
        financialReportMap.put("description", getDescription());
        if (getProgram() != null) {
            financialReportMap.put("idProgram", getProgram().getId());
            financialReportMap.put("programName", getProgram().getName());
        }
        if (getCoa() != null) {
            financialReportMap.put("idCoa", getCoa().getId());
            financialReportMap.put("coaName", getCoa().getName());
        }
        return financialReportMap;
    }
}