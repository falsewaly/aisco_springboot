package amanah.codebase.model;

import java.util.HashMap;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="financialreport_incomefrequency")
public class FinancialReportExpenseFrequency extends FinancialReportExpense{
    private String frequency;

    public FinancialReportExpenseFrequency(String datestamp, long amount,
                                 String description, Program program, ChartOfAccount coa,
                                 String frequency) {
    	super(datestamp, amount, description, program, coa);
        this.frequency = frequency;
    }

    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> financialReportMap = super.toHashMap();
        financialReportMap.put("frequency", getFrequency());
        return financialReportMap;
    }
}