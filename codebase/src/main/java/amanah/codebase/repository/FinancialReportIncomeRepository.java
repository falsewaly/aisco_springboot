package amanah.codebase.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import amanah.codebase.model.FinancialReportIncome;

public interface FinancialReportIncomeRepository extends JpaRepository<FinancialReportIncome, Integer>{

}
