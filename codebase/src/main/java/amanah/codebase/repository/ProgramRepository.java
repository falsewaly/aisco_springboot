package amanah.codebase.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import amanah.codebase.model.Program;

public interface ProgramRepository extends JpaRepository<Program, Integer>{

}
