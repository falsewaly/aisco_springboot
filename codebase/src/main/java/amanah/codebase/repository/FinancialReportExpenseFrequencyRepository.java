package amanah.codebase.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import amanah.codebase.model.FinancialReportExpenseFrequency;

public interface FinancialReportExpenseFrequencyRepository extends JpaRepository<FinancialReportExpenseFrequency, Integer>{

}
