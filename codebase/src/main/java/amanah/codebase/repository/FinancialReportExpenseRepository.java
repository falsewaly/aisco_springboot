package amanah.codebase.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import amanah.codebase.model.FinancialReportExpense;

public interface FinancialReportExpenseRepository extends JpaRepository<FinancialReportExpense, Integer>{

}
