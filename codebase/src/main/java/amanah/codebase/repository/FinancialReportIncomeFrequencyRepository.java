package amanah.codebase.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import amanah.codebase.model.FinancialReportIncomeFrequency;

public interface FinancialReportIncomeFrequencyRepository extends JpaRepository<FinancialReportIncomeFrequency, Integer>{

}
