package amanah.codebase.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import amanah.codebase.model.AutomaticReportPeriodic;

public interface AutomaticReportPeriodicRepository extends JpaRepository<AutomaticReportPeriodic, Integer> {
}
