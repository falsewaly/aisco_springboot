package amanah.product.samuelcharity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import amanah.product.samuelcharity.model.FinancialReportIncome;

public interface FinancialReportIncomeRepository extends JpaRepository<FinancialReportIncome, Integer>{

}
