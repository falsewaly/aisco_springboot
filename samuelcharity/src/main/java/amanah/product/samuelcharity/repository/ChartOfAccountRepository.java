package amanah.product.samuelcharity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import amanah.product.samuelcharity.model.ChartOfAccount;

public interface ChartOfAccountRepository extends JpaRepository<ChartOfAccount, Integer>{

}
