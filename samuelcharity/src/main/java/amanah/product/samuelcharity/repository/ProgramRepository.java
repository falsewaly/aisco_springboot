package amanah.product.samuelcharity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import amanah.product.samuelcharity.model.Program;

public interface ProgramRepository extends JpaRepository<Program, Integer>{

}
