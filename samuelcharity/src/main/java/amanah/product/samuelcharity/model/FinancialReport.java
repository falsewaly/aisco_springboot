package amanah.product.samuelcharity.model;

import java.util.HashMap;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

@Data
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public abstract class FinancialReport {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="seq")
    @GenericGenerator(name = "seq", strategy="increment")
    protected int id;
    protected String datestamp;
    protected long amount;
    protected String description;
    @ManyToOne
    protected Program program;
    @ManyToOne
    protected ChartOfAccount coa;

    public abstract HashMap<String, Object> toHashMap();
}
