package amanah.product.samuelcharity.resource;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import amanah.product.samuelcharity.model.ChartOfAccount;
import amanah.product.samuelcharity.model.FinancialReportIncome;
import amanah.product.samuelcharity.model.Program;
import amanah.product.samuelcharity.repository.ChartOfAccountRepository;
import amanah.product.samuelcharity.repository.FinancialReportIncomeRepository;
import amanah.product.samuelcharity.repository.ProgramRepository;

@RestController
public class FinancialReportIncomeResource {

	@Autowired
    private FinancialReportIncomeRepository repository;
    @Autowired
    private ProgramRepository programRepository;
    @Autowired
    private ChartOfAccountRepository coaRepository;

    @PostMapping("call/income/save")
    public List<FinancialReportIncome> saveFinancialReportIncome(@RequestBody HashMap<String, String>
    financialReportData) {
        String datestamp = financialReportData.get("datestamp");
        long amount = Long.parseLong(financialReportData.get("amount"));
        String description = financialReportData.get("description");
        Program program = programRepository.findById(Integer.parseInt(financialReportData
            .get("idProgram"))).orElse(null);
        ChartOfAccount coa = coaRepository.findById(Integer.parseInt(financialReportData
            .get("idCoa"))).orElse(null);
        String paymentMethod = financialReportData.get("paymentMethod");
        FinancialReportIncome financialReport = new FinancialReportIncome(
            datestamp, amount, description, program, coa, paymentMethod
        );

        repository.save(financialReport);
        return getAllFinancialReportIncome();
    }
	
	    @PutMapping("call/income/update")
	    public FinancialReportIncome updateFinancialReportIncome(@RequestBody HashMap<String, String>
	                                                                financialReport) {
	    	FinancialReportIncome existingFinancialReport =
	                repository.findById(Integer.parseInt(financialReport.get("id")))
	                    .orElse(null);
	        existingFinancialReport.setDatestamp(financialReport.get("datestamp"));
	        existingFinancialReport.setAmount(Long.parseLong(financialReport.get("amount")));
	        existingFinancialReport.setDescription(financialReport.get("description"));
	        Program program = programRepository.findById(Integer.parseInt(financialReport
	            .get("idProgram"))).orElse(null);
	        ChartOfAccount coa = coaRepository.findById(Integer.parseInt(financialReport
	            .get("idCoa"))).orElse(null);
	
	        existingFinancialReport.setProgram(program);
	        existingFinancialReport.setCoa(coa);
	        existingFinancialReport.setPaymentMethod(financialReport.get("paymentMethod"));
	        return repository.save(existingFinancialReport);
	    }
	
	    @GetMapping("call/income/detail")
	    public FinancialReportIncome getFinancialReportIncome(@RequestParam int id) {
	    	return repository.findById(id).orElse(null);
	    }
	
	    @GetMapping("call/income/list")
	    public List<FinancialReportIncome> getAllFinancialReportIncome() {
	    	return repository.findAll();
	    }
	
	    @DeleteMapping("call/income/delete")
	    public List<FinancialReportIncome> deleteFinancialReportIncome(@RequestBody HashMap<String,
	        String> idMap) {
	        int id = Integer.parseInt(idMap.get("id"));
	        repository.deleteById(id);
	        return getAllFinancialReportIncome();
	    }

}
