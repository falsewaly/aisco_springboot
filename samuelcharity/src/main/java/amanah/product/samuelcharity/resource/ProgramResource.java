package amanah.product.samuelcharity.resource;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import amanah.product.samuelcharity.model.Program;
import amanah.product.samuelcharity.repository.ProgramRepository;

@RestController
public class ProgramResource {

	@Autowired
	private ProgramRepository repository;
	
	@PostMapping("call/program/save")
    public List<Program> saveProgram(@RequestBody Program program) {
		repository.save(program);
        return getAllProgram();
    }

	@PutMapping("call/program/update")
    public Program updateProgram(@RequestBody Program program) {
		Program existingProgram = repository.findById(program.getId()).orElse(null);
		existingProgram.setName(program.getName());
		existingProgram.setDescription(program.getDescription());
		existingProgram.setTarget(program.getTarget());
		existingProgram.setPartner(program.getPartner());
		existingProgram.setLogoUrl(program.getLogoUrl());
		existingProgram.setExecutionDate(program.getExecutionDate());
		return repository.save(existingProgram);
    }

    @GetMapping("call/program/detail")
    public Program getProgram(@RequestParam int id) {
    	return repository.findById(id).orElse(null);
    }

    @GetMapping("call/program/list")
    public List<Program> getAllProgram() {
    	return repository.findAll();
    }

    @DeleteMapping("call/program/delete")
    public List<Program> deleteProgram(@RequestBody HashMap<String,
            String> idMap) {
        int id = Integer.parseInt(idMap.get("id"));
    	repository.deleteById(id);
        return getAllProgram();
    }

}
